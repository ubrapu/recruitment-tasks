# Recruitment tasks

The purpose of the repository is to collect recruitment tasks that I completed for companies.

Recruitment tasks for companies:
| Companies | Software | Skills | For sports, extended beyond the requirements with |
| :-------: | :------: | :----: | :----------------------------------------------:  |
| [Scramjet](Scramjet/README.md) | Docker, Helm, Jenkins |  |  |
| [Svarmony](Svarmony/README.md) | Docker | Programming (Go, C++) |  |
| [T2G Europe](T2G_Europe/README.md) | Docker |  |  |
