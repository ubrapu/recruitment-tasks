This program was created in C++.

Its purpose is to generate a list from a given string with a certain number of permutations.

The generated lists will be used in another program.

C++ is faster than Golang, so it was used to generate the list.