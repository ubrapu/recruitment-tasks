Write a program that automatically sends requests based on randomly selected parameters with a maximum of two characters and for second case with three characters.

Recommended language: Golang >=v16.
Reason: Experience with BurpSuite

## Short

Example URL for two characters:
https://ionaapp.com/assignment-magic/dk/short/2d

Response in JSON:
```
{"uid":"a35aea60fe097c885568babb48ee7d1e"}
```

We have ```62*62=3844``` combination.

## Long

Example URL for three characters:
https://ionaapp.com/assignment-magic/dk/long/ab2

Response in JSON:
```
{"uid":"53f1ce1310367adfe34e70e39c454d88"}
```

We have ```62*62*62=238328``` combination.
