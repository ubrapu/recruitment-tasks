# For Svarmony company
- [Company website](https://svarmony.com/)
- [Company LinkedIn](https://www.linkedin.com/company/svarmony/)

In this folder you will find a recruitment task performed for the company in accordance with the attached instructions.

Instructions for tasks can be found in the folders of individual tasks.

Folder with task
- [First task](task_1/README.md)
- [Second task](task_2/README.md)