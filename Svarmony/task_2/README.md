# Task 2

Instruction for this task can be found in the markdown file below.
- [Instruction](instruction.md)

The instruction was intended to allow me to prepare for the call.
All solutions were to be proposed during the call, while the file below was only for my personal notes so that I would know what to talk about.
- [Solution](solution.md)